<?php

namespace CSTClasses_A;

/**
 * The PasswordChecker allows us to check if an entered username and
 * password is valid.
 *
 * @author ins214
 */
class PasswordChecker
{
    /**
     * Purpose: Determine whether the supplied username and password
     *   combination is valid
     * @param string $username The username to check
     * @param string $password The password to check
     * @return boolean TRUE if the username / password combination is valid,
     *   and FALSE otherwise
     */
    public function isValid( $username, $password )
    {
        // By default, the combination is not valid
        $valid = FALSE;
        
        // Open a database connection
        $db = new DbObject();
        
        // Query for the password for the specified username
        $qryResult = $db->select( "password", "Password",
            "username='" . $db->escape( $username ) . "'" );

        // If there was one row returned, check the password against
        // the supplied password
        if ( $qryResult->num_rows == 1 )
        {
            $passwordRow = $qryResult->fetch_row();
            if ( password_verify( $password, $passwordRow[0] ) )
            {
                $valid = TRUE;
            }
        }
        
        // Return whether the username and password combination is valid
        return $valid;
    }

    /**
     * Purpose: Add a user into the password list
     * @param string $username The username to add
     * @param string $password The password associated with the username
     * @return boolean TRUE if the user was successfully added,
     *   FALSE otherwise
     */
    public function addUser( $username, $password )
    {
        // Open a database connection
        $db = new DbObject();

        // Create the array to use with the insert method
        $record["username"] = $db->escape( $username );
        $record["password"] = password_hash( $password, PASSWORD_DEFAULT );
        
        // Verify that the user does not currently exist in the database
        $qryResult = $db->select("username", "Password",
                "username='" . $db->escape( $username ) . "'" );
        $numExisting = $qryResult->num_rows;
        $qryResult->free();
        
        // If there was a user with that name
        if ( $numExisting > 0 )
        {
            // We weren't successful in adding the user
            $addResult = FALSE;
        }
        else
        {
            // Insert the user into the Password database
            $numAdded = $db->insert( $record, "Password" );
            
            // We were only successful if we inserted a single row
            $addResult = ( $numAdded == 1 );
        }

        return $addResult;
    }

    
            }
