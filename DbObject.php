<?php

namespace CSTClasses_A;

/**
 * The DbObject class represents a connection to a MySQL server.
 * With objects of this class, we'll be able to create and execute
 * query statements.  This is a convenience class, wrapped around
 * the mysqli object.
 *
 * @author ins214
 */
class DbObject
{
    /**
     * The database connection
     */
    private $dbConnect;
    
    /**
     * Purpose: To create a connection to a MySQL server and
     *   open a database on that server.
     * @param string $server - name of the MySQL server
     * @param string $user - name of the MySQL user
     * @param string $password - user's password
     * @param string $schema - name of the schema to use
     */
    public function __construct( $server = "kelcstu06", $user = "INS214",
            $password = "RASSLER", $schema = "INS214" )
    {
        // Create a mysqli object, and assign it to the internal attribute
        $this->dbConnect = new \mysqli( $server, $user, $password, $schema );
        
        // If the connection failed
        if ( $this->dbConnect->connect_error )
        {
        // Then
            // Display an error message
            // Exit
            echo "<p>Failed to connect to database: " .
                    $this->dbConnect->connect_error . "</p>\n";
            exit();
        }
    }
    
    /**
     * Purpose: This routine will run the query that is provided by the query
     *   string that is passed in.  If the query fails, we will stop
     *   processing and exit the PHP interpreter immediately.
     * @param string $qryString The SQL query string that is to be run
     * @return mysqli_result The result of the query if the query is
     *   successful and the query is SELECT, DESCRIBE, EXPLAIN, or SHOW,
     *   or TRUE if the query is successful and is not one of those queries.
     *   The function will stop processing and exit the PHP interpreter
     *   if the query fails.
     */
    public function runQuery( $qryString )
    {
        // Execute the query
        $qryResult = $this->dbConnect->query( $qryString );
        
        // If the query failed
        if ( !$qryResult )
        {
            // Print an error message, then exit
            echo "Query $qryString couldn't execute\n";
            die();
        }
        
        // Return the result of the query
        return $qryResult;
    }
    
    /**
     * Purpose: Perform a SELECT query on the database
     * @param string $columnList list of columns to be selected
     * @param string $tableList list of tables to select from
     * @param string $condition optional SQL condition to select with
     * @param string $sort optional SQL sort statement to apply
     * @param string $other optional any other SQL clauses to apply
     * @return mysqli_result The result of the SELECT query
     */
    public function select( $columnList, $tableList,
            $condition="", $sort="", $other="" )
    {
        // Create the basic SELECT statement
        $qryStmt = "SELECT $columnList FROM $tableList";
        
        // If a condition is specified, add it to the query
        if ( $condition != "" ) {
            $qryStmt .= " WHERE $condition";
        }
        
        // If a sort order is specified, add it to the query
        if ( $sort != "" ) {
            $qryStmt .= " ORDER BY $sort";
        }
        
        // Add any other SQL clauses if they've been specified
        if ( $other != "" ) {
            $qryStmt .= " $other";
        }
        
        echo "<div>SELECT query to be executed: $qryStmt</div>\n";
        
        // Execute the query, and return the result
        return $this->runQuery( $qryStmt );
    }
    
   /**
     * Purpose: This method will add a new record to the specified table
     * @param array $newRecord An associative array of the field names
     *   (the array index) and the values to be inserted (the array values)
     * @param string $tableName The name of the table to add the record to
     * @return int The number of rows inserted
     */
    function insert( $newRecord, $tableName )
    {
        // Construct the field name and values lists
        $fieldList = "( ";
        $valueList = "( ";
        
        foreach ( $newRecord as $field=>$value )
        {
            $fieldList .= $field . ", ";
            // Don't forget to escape the user-supplied value, in order
            // to prevent an SQL injection attack!!!
            $valueList .= "'" .
                $this->dbConnect->real_escape_string( $value ) . "', ";
        }
        
        // We've finished adding all the field names and values to their
        // respective lists, so delete the final comma and space.
        $fieldList = rtrim( $fieldList, ", " );
        $valueList = rtrim( $valueList, ", " );

        // Add the closing parenthesis to the lists
        $fieldList .= " )";
        $valueList .= " )";
        
        // Perform the insertion
        $insStatement = "INSERT INTO " . $tableName . " " . $fieldList .
            " VALUES " . $valueList;
        // print "Insert statement is: " . $insStatement . "<br />\n";
        // return 1;
        $this->runQuery( $insStatement );
        
        // Return the number of affected rows
        return $this->dbConnect->affected_rows;
    }

    /**
     * Purpose: Updates a record in the specified table with the values
     *   passed in.
     * Assumption: There is a value for the primary key in the values array,
     *   and we don't want to update that.
     * @param array $values An associative array of field names and the values
     *   that those fields are to be updated to
     * @param string $tableName The name of the table to update
     * @param string $primaryKey The name of the primary key
     * @return int The number of rows updated
     */
    public function update( $values, $tableName, $primaryKey ) {
        // Specify which table to update
        $updateStatement = "UPDATE $tableName SET ";
        
        // LOOP for each of the passed in fields
        foreach ( $values as $fieldName=>$fieldValue )
        {
            // If this isn't the primary key
            if ( $fieldName != $primaryKey )
            {
                // Add the column and value to be changed to the query statment
                $updateStatement .= $fieldName . "='" .
                        $this->dbConnect->real_escape_string( $fieldValue ) .
                        "', ";
            }
        }
        
        // Oops, we added in one too many commas -- remove it
        $updateStatement = rtrim( $updateStatement, ", " );
        
        // Add in the condition to specify which record to update
        $updateStatement .= " WHERE " . $primaryKey . "='" .
            $this->dbConnect->real_escape_string( $values[$primaryKey] ) .
            "'";
        
        // Debugging: print out the query statement
        // echo "<p>update statement: $updateStatement</p>\n";
        // return 1;
        
        // Run the query
        $this->runQuery( $updateStatement );
        
        // Return the number of rows affected by the last query
        return $this->dbConnect->affected_rows;
    }

    /**
     * Purpose: Prepare an SQL statement for execution by our database
     *   connection
     * @param string $query The query to be prepared
     * @return mysqli_stmt The prepared statement, or FALSE if an error
     *   occurred
     */
    public function prepare( $query )
    {
        return $this->dbConnect->prepare( $query );
    }
    
    /**
     * Purpose: Escapes special characters in a string for use in an SQL
     *   statement.
     * @param string $strToEscape  The string to be escaped
     * @return string The escaped string
     */
    public function escape( $strToEscape )
    {
        return $this->dbConnect->real_escape_string( $strToEscape );
    }
    
    /**
     * Purpose: Creates an associative array to be used with the Formitron
     *   objects that populate lists
     * @param mysqli_result $qryResults The query result record set.  The
     *   result should consist of two columns: the first column will contain
     *   an ID, and the second column will contain the corresponding text.
     * @return array An associative array, where the array index comes from
     *   the qryResult's first column, and the array value comes from the
     *   qryResult's second column.
     */
    static public function createArray( $qryResults )
    {
        // Create an empty result array
        $result = array();
        
        // Loop through all rows in the result set
        while ( $row = $qryResults->fetch_row() )
        {
            $result[$row[0]] = $row[1];
        }
        
        // Return the result array
        return $result;
    }
    
    /**
     * Purpose: Display the results of a database query in an HTML table
     * @param mysqli_result $qryResults Results of a previous database query
     */
    public static function displayRecords( $qryResults )
    {
        // Display the opening table tag
        echo "<table class='table table-striped'>\n";
        
        // Display a thead opening tag and a table row opening tag
        echo "  <thead>\n";
        echo "  <tr>";
        
        // LOOP for all query result columns
        foreach ( $qryResults->fetch_fields() as $fieldInfo )
        {
            // Display the column name within a table header tag
            echo "<th>" . htmlspecialchars( $fieldInfo->name ) . "</th>";
        }

        // Display a table row closing tag and a thead closing tag
        echo "</tr>\n";
        echo "  </thead>\n";
        
        // Display a tbody opening tag
        echo "  <tbody>\n";
        
        // LOOP for all the query rows returned
        while ( $row = $qryResults->fetch_row() )
        {
            // Display a table row opening tag
            echo "    <tr>";
            
            // LOOP for all the query result columns
            for ( $i = 0; $i < $qryResults->field_count; $i++ )
            {
		// Display the value of this query result row and column
		// 	within a table data tag
                echo "<td>" . htmlspecialchars( $row[$i] ) . "</td>";
            }
            // Display a table row closing tag
            echo "</tr>\n";
        }
        
        // Display a tbody closing tag
        echo "  </tbody>\n";
        
        // Display a table closing tag
        echo "</table>\n";
    }

    /**
     * Purpose: Close the database connection.
     * The destructor gets called when the object goes out of scope
     * (function terminates, program ends).  Rather than having a separate
     * close method (which might be a good idea anyways, because then we can
     * close early if we want), we'll just close the connection here.
     */
    public function __destruct()
    {
        $this->dbConnect->close();
    }
}
